# Individualized Application
Modulate music as a set of templates and customized beat properties to parameterize a full beat-synced workout from a collection of music

*   Auto-populate playlist from existing collection
    *   Maybe fill-in songs from third-party streaming service OF CHOICE
    *   allow option for GraphQL-based API for adding songs to local database for analysis