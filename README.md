# BeatSync
Work with Opal Soteria on a Heart/BeatSync application to enhance rhythmic dance rituals in a specific synced modality such that a target heart rate is achieved with an audio feedback loop of 'heart-healthy' music that changes in tempo and pitch to compensate for time-based sonic distortions

- [Charity](./BeatSync/Charity.md)<br/> 

- [Individualized Application](./BeatSync/Individualized-Application.md)